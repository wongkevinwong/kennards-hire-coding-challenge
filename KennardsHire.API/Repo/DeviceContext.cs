﻿using Microsoft.EntityFrameworkCore;
using System;

namespace KennardsHire.API.Repo
{
    public class DeviceContext : DbContext
    {
        public DeviceContext(DbContextOptions<DeviceContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        public DbSet<Model.DeviceModel> Devices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model.DeviceModel>().HasData(
                new Model.DeviceModel
                {
                    Id = 1,
                    DeviceVendor = "vendor A",
                    DeviceType = "type A",
                    FriendlyName = "Hammer",
                    AssetType = "Lease",
                    BranchId = 1,
                    BranchName = "Branch A",
                    LastSeen = DateTime.UtcNow
                },
                new Model.DeviceModel
                {
                    Id = 2,
                    DeviceVendor = "vendor B",
                    DeviceType = "type B",
                    FriendlyName = "Cart",
                    AssetType = "Lease",
                    BranchId = 2,
                    BranchName = "Branch B",
                    LastSeen = DateTime.UtcNow
                },
                new Model.DeviceModel
                {
                    Id = 3,
                    DeviceVendor = "vendor C",
                    DeviceType = "type C",
                    FriendlyName = "Saw",
                    AssetType = "Lease",
                    BranchId = 3,
                    BranchName = "Branch C",
                    LastSeen = DateTime.UtcNow
                },
                new Model.DeviceModel
                {
                    Id = 4,
                    DeviceVendor = "vendor D",
                    DeviceType = "type D",
                    FriendlyName = "Screw driver",
                    AssetType = "Lease",
                    BranchId = 4,
                    BranchName = "Branch D",
                    LastSeen = DateTime.UtcNow
                }
            );
        }
    }
}
