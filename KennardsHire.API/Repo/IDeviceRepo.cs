﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace KennardsHire.API.Repo
{
    public interface IDeviceRepo
    {
        Task<IEnumerable<Model.DeviceModel>> GetAllAsync();

        Task<Model.DeviceModel> GetByIdAsync(int id);

        Task UpdateAsync(Model.DeviceModel merchant);


    }
}
