﻿using KennardsHire.API.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KennardsHire.API.Repo
{
    public class DeviceRepo : IDeviceRepo
    {
        private readonly DeviceContext _deviceDb;

        public DeviceRepo(DeviceContext deviceDb) => this._deviceDb = deviceDb;

        public async Task<IEnumerable<DeviceModel>> GetAllAsync()
        {
            return await _deviceDb.Devices.ToListAsync();
        }

        public async Task<DeviceModel> GetByIdAsync(int id)
        {
            var merchant = await _deviceDb.Devices.FindAsync(id);

            if (merchant == null)
            {
                return null;
            }

            return merchant;
        }

        public async Task UpdateAsync(DeviceModel merchant)
        {
            _deviceDb.Entry(merchant).State = EntityState.Modified;

            await _deviceDb.SaveChangesAsync();
        }
    }
}
