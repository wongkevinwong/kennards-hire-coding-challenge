﻿using System;

namespace KennardsHire.API.Model
{
    public class DeviceModel
    {
        public int Id { get; set; }

        public string DeviceVendor { get; set; }

        public string DeviceType { get; set; }

        public string FriendlyName { get; set; }

        public string AssetType { get; set; }

        public int BranchId { get; set; }

        public string BranchName { get; set; }

        public DateTime LastSeen { get; set; }
    }
}
