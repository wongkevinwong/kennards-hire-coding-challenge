﻿using KennardsHire.API.Repo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace KennardsHire.API.Controllers
{
    [EnableCors]
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly IDeviceRepo _deviceRepo;

        public DeviceController(IDeviceRepo deviceRepo)
        {
            this._deviceRepo = deviceRepo;
        }

        [HttpGet()]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var devices = await _deviceRepo.GetAllAsync();

                if (!devices.Any())
                {
                    return new NoContentResult();
                }
                return new ObjectResult(devices);
            }
            catch (Exception ex)
            {
                throw new HttpRequestException("GET error", ex);
            }

        }

        [HttpGet("{id}", Name = "GetById")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var device = await _deviceRepo.GetByIdAsync(id);

                if (device == null)
                {
                    return new NotFoundObjectResult(null);
                }
                else
                {
                    return new ObjectResult(device);
                }
            }
            catch (Exception ex)
            {
                throw new HttpRequestException("GET error", ex);
            }
        }


        public async Task<IActionResult> Put([FromBody] Model.DeviceModel device)
        {
            try
            {
                var existingMerchant = await _deviceRepo.GetByIdAsync(device.Id);

                if (existingMerchant == null)
                    return new NotFoundObjectResult(null);
                else
                {
                    await _deviceRepo.UpdateAsync(device);
                    return new ObjectResult(device);
                }
            }
            catch (Exception ex)
            {
                throw new HttpRequestException("PUT error", ex);
            }
        }

    }
}
