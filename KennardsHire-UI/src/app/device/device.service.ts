import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  combineLatest,
  Observable,
  of,
  Subject,
  throwError,
} from 'rxjs';
import { catchError, map, shareReplay, tap, toArray } from 'rxjs/operators';
import { Branch } from './branch';
import { Device } from './device';

@Injectable({
  providedIn: 'root',
})
export class DeviceService {
  private deviceUrl = 'http://localhost:5000/api/device';

  constructor(private http: HttpClient) {}

  private deviceSelectedSubject = new BehaviorSubject<number>(0);
  deviceSelectedAction$ = this.deviceSelectedSubject.asObservable();

  private deviceUpdatedSubject = new BehaviorSubject<Device>({
    id: -1,
  } as Device);
  deviceUpdatedAction$ = this.deviceUpdatedSubject.asObservable();

  branches$ = of([
    { id: 1, name: 'branch A' },
    { id: 2, name: 'branch B' },
    { id: 3, name: 'branch C' },
    { id: 4, name: 'branch D' },
  ] as Branch[]);

  devices$ = combineLatest([
    this.http.get<Device[]>(this.deviceUrl).pipe(
      tap((data) => console.log('Devices', JSON.stringify(data))),
      shareReplay(1),
      catchError(this.handleError)
    ),
    this.deviceUpdatedAction$,
    this.branches$,
  ]).pipe(
    map(([devices, updatedDevice, branches]) =>
      devices.map((device) =>
        updatedDevice && +updatedDevice.id === +device.id
          ? ({
              ...updatedDevice,
              branchName: branches.find((b) => b.id === updatedDevice.branchId)
                .name,
            } as Device)
          : ({
              ...device,
              branchName: branches.find((b) => b.id === device.branchId).name,
            } as Device)
      )
    )
  );

  selectedDevice$ = combineLatest([
    this.devices$,
    this.deviceSelectedAction$,
  ]).pipe(
    map(([devices, selectedDeviceId]) =>
      devices.find((device) => device.id === +selectedDeviceId)
    ),
    tap((device) => console.log('selected device', device)),
    shareReplay(1)
  );

  setSelectedDevice(deviceId: number): void {
    this.deviceSelectedSubject.next(deviceId);
  }

  updateDevice(updatedDevice: Device): void {
    this.deviceUpdatedSubject.next(updatedDevice);
    // updatedDevices$ = combineLatest([
    //   this.devices$,
    //   updatedDevice$,
    //   this.branches$,
    // ]).pipe(
    //   map(([devices, updatedDevice, branches]) =>
    //     devices.map((device) => {
    //       if (device.id === updatedDevice.id) {
    //         return {
    //           ...updatedDevice,
    //           branchName: branches.find((b) => b.id === updatedDevice.branchId)
    //             .name,
    //         } as Device;
    //       } else {
    //         return {
    //           ...device,
    //           branchName: branches.find((b) => b.id === device.branchId).name,
    //         } as Device;
    //       }
    //     })
    //   ),
    //   shareReplay(1)
    // );
  }

  selectedBranchChanged(selectedBranchId: number): void {
    this.devices$ = combineLatest([
      this.http.get<Device[]>(this.deviceUrl).pipe(
        tap((data) => console.log('Devices', JSON.stringify(data))),
        catchError(this.handleError),
        shareReplay(1)
      ),
      this.branches$,
    ]).pipe(
      map(([devices, branches]) =>
        devices.map(
          (device) =>
            ({
              ...device,
              branchId: selectedBranchId,
              branchName: branches.find((b) => b.id === +selectedBranchId).name,
            } as Device)
        )
      )
    );
  }

  private handleError(err: any): Observable<never> {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }
}
