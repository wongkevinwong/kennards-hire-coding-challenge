import { Component, ChangeDetectionStrategy } from '@angular/core';
import { EMPTY, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DeviceService } from './device.service';
import { MatDialog } from '@angular/material/dialog';
import { DeviceEditComponent } from './device-edit.component';
import { Device } from './device';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeviceListComponent {
  pageTitle = 'Device List';

  devices: Device[];

  constructor(private deviceService: DeviceService, public dialog: MatDialog) {}

  private errorMessageSubject = new Subject<string>();
  errorMessage$ = this.errorMessageSubject.asObservable();

  devices$ = this.deviceService.devices$.pipe(
    catchError((err) => {
      this.errorMessageSubject.next(err);
      return EMPTY;
    })
  );

  displayPopUp(deviceId: number): void {
    this.dialog.open(DeviceEditComponent, {
      data: {
        deviceId,
      },
    });
  }
}
