export interface Device {
  id: number;
  deviceVendor?: string;
  deviceType?: string;
  friendlyName?: string;
  assetType?: string;
  branchId?: number;
  branchName?: string;
  lastSeen?: string;
}
