import { Component, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { combineLatest, EMPTY, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Branch } from './branch';
import { Device } from './device';
import { DeviceService } from './device.service';

@Component({
  selector: 'app-device-edit',
  templateUrl: './device-edit.component.html',
  styleUrls: ['./device-edit.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeviceEditComponent {
  selectedDeviceId: number;

  private errorMessageSubject = new Subject<string>();
  errorMessage$ = this.errorMessageSubject.asObservable();

  private deviceSubject = new Subject<Device>();
  deviceObservable$ = this.deviceSubject.asObservable();

  selectedDevice$ = this.deviceService.selectedDevice$.pipe(
    catchError((err) => {
      this.errorMessageSubject.next(err);
      return EMPTY;
    })
  );

  vm$ = combineLatest([
    this.selectedDevice$,
    this.deviceService.branches$,
  ]).pipe(
    map(([device, branches]: [Device, Branch[]]) => ({
      device,
      branches,
    }))
  );

  onSave(): void {
    this.selectedDevice$
      .pipe(map((device) => this.deviceService.updateDevice(device)))
      .subscribe(() => this.dialogRef.close());
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSelected(productId: number): void {
    this.deviceService.selectedBranchChanged(productId);
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private dialogRef: MatDialogRef<DeviceEditComponent>,
    private deviceService: DeviceService
  ) {
    this.selectedDeviceId = +this.dialogData.deviceId;
    this.deviceService.setSelectedDevice(this.selectedDeviceId);
  }
}
