import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DeviceListComponent } from './device/device-list.component';
import { DeviceService } from './device/device.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { DeviceEditComponent } from './device/device-edit.component';

@NgModule({
  declarations: [AppComponent, DeviceListComponent, DeviceEditComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
  ],
  bootstrap: [AppComponent],
  providers: [DeviceService],
})
export class AppModule {}
